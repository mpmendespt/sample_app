# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
SampleApp::Application.config.secret_key_base = 'b2bbde1b34c0ea79633f70b410e872fa1abdb265f484dafa0e3819c1bef835bee9a2fc419a237d6ca426eafd8b3699b1686ac6cc5518c1e00fb2ffe16f44d028'
